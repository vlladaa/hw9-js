
/*
--- теорія ---

1) новий HTML-тег можна створити використовуючи, крім самого HTML-файлу, метод .createElement(), усередині дужок прописавши необхідний тег
2) перший параметр цієї функції вказує місце (позицію), куди буде доданий html з другої її частини. можливі такі варіанти:
   - beforebegin (додає код перед елементом)
   - afterbegin (додає код на початок елементу всередину нього)
   - beforeend (додає код в кінець елементу всередину нього)
   - afterend (додає код після елементу)
3) спочатку потрібно через методи вказати елемент, який потрібно видалити, після чого викликати на цьому елементі метод remove()

*/


const randomArr = ["hello", "world", "Kyiv", "Kharkiv", "Odessa", "Lviv"];
const randomArrSecond = ["1", "2", "3", "sea", "user", 23];

const createList = (arr, domEl = document.body) => {
  const ol = document.createElement('ol')
  arr.forEach(element => {
    const li = document.createElement('li');
    li.innerText = element;
    ol.appendChild(li);
  });

  domEl.appendChild(ol)

}

createList(randomArr);